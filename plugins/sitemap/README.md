El complemento para Pelican sitemap se encuentra bajo la licencia GNU
Affero General Public License, version 3. Fue obtenido de
<https://github.com/getpelican/pelican-plugins>.
