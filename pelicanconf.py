#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

from pelican import __version__ as PELICAN_VERSION


# Basic settings (http://docs.getpelican.com/en/stable/settings.html#basic-settings)
DEFAULT_CATEGORY = 'Sin categoría'
DELETE_OUTPUT_DIRECTORY = False
DISPLAY_CATEGORIES_ON_MENU = True
DISPLAY_PAGES_ON_MENU = True
MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.codehilite': {'css_class': 'highlight'},
        'markdown.extensions.extra': {},
        'markdown.extensions.footnotes': {'BACKLINK_TITLE': 'Volver a la nota %d en el texto'},
        'markdown.extensions.meta': {},
    },
    'output_format': 'html5',
}
# Fix issue with < and > characters (among others) being double-escaped
# Bug report in markdown/extensions/codehilite.py → https://github.com/Python-Markdown/markdown/pull/726/files

PATH = 'content'
PLUGIN_PATHS = ['plugins']
PLUGINS = ['another_read_more_link', 'compressor', 'i18n_subsites', 'neighbors', 'pelican-css-js', 'sitemap', 'tag-cloud', 'tipue-search']
# PAGE_PATHS = ['pages']
# PAGE_URL = 'pages/{slug}/'
# PAGE_SAVE_AS = 'pages/{slug}/index.html'
SITENAME = 'LibreTube'
SITENAME_SINGLE = 'CL'
SITEURL = 'https://lablibre.tuxfamily.org'

# URL settings (http://docs.getpelican.com/en/stable/settings.html#url-settings)
RELATIVE_URLS = True
ARTICLE_PATHS = ['articles']
ARTICLE_URL = '{slug}/'
ARTICLE_SAVE_AS = '{slug}/index.html'
CATEGORY_URL = 'category/{slug}/'
CATEGORY_SAVE_AS = 'category/{slug}/index.html'
CATEGORIES_URL = 'categories/'
CATEGORIES_SAVE_AS = 'categories/index.html'
TAG_URL      = 'tag/{slug}/'
TAG_SAVE_AS  = 'tag/{slug}/index.html'
TAGS_URL     = 'tags/'
TAGS_SAVE_AS = 'tags/index.html'
AUTHOR_URL      = 'author/{slug}/'
AUTHOR_SAVE_AS  = 'author/{slug}/index.html'
YEAR_ARCHIVE_SAVE_AS = 'archives/{date:%Y}/index.html'
MONTH_ARCHIVE_SAVE_AS = 'archives/{date:%Y}/{date:%m}/index.html'
AUTHORS_URL     = 'pages/créditos/'
AUTHORS_SAVE_AS = 'pages/créditos/index.html'
ARCHIVES_URL     = 'archives/'
ARCHIVES_SAVE_AS = 'archives/index.html'
STATIC_PATHS = ['asciicasts', 'heckyel_pub.asc', 'robots.txt', 'wp-content']
PAGE_EXCLUDES = ['asciicasts', 'wp-content']
ARTICLE_EXCLUDES = ['asciicasts', 'wp-content']

# Time and date (http://docs.getpelican.com/en/stable/settings.html#time-and-date)
TIMEZONE = 'Europe/Madrid'
LOCALE = ('es_ES.UTF-8')

# Feed settings (http://docs.getpelican.com/en/stable/settings.html#feed-settings)
# feed generation is usually not desired when developing, set to true in publishconf.py
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Pagination (http://docs.getpelican.com/en/stable/settings.html#pagination)
DEFAULT_PAGINATION = 12
PAGINATION_PATTERNS = (
    (1, '{base_name}/', '{base_name}/index.html'),
    (2, '{base_name}/page/{number}/', '{base_name}/page/{number}/index.html'),
)
PAGINATOR_LIMIT = 5

# Translations (http://docs.getpelican.com/en/stable/settings.html#translations)
DEFAULT_LANG = 'es'
TRANSLATION_FEED_ATOM = None

# Themes (http://docs.getpelican.com/en/stable/settings.html#themes)
THEME = 'libretube-theme/'
SITESUBTITLE = 'Sitio de información sobre Software Libre'
SHOW_RECENT_POSTS = 9  # the number of recent posts to show


# Plugins' configuration (not from Pelican core)
TAG_CLOUD_STEPS = 5
TAG_CLOUD_MAX_ITEMS = 53
TAG_CLOUD_SORTING = 'random'
TAG_CLOUD_BADGE = True

SITEMAP = {
    'format': 'xml',
    'priorities': {
        'articles': 0.5,
        'indexes': 0.5,
        'pages': 0.5
    },
    'changefreqs': {
        'articles': 'yearly',
        'indexes': 'weekly',
        'pages': 'yearly'
    }
}

SOCIAL = {
    'diaspora': '#',
    'gnusocial': '#',
    'mastodom': '#',
    'matrix': '#',
    'mediagoblin': '#',
    'peertube': '#',
    'pixelfed': '#',
    'pump': '#'
}

LICENSE = {

    'allrights': {
        'name': 'All rights reserved'
    },
    'ccby4': {
        'name': 'CC BY 4.0',
        'fullname': 'Attribution 4.0 International',
        'url': 'https://creativecommons.org/licenses/by/4.0/'
    },
    'ccbysa4': {
        'name': 'CC BY-SA 4.0',
        'fullname': 'Attribution-ShareAlike 4.0 International',
        'url': 'https://creativecommons.org/licenses/by-sa/4.0/'
    },

    'ccbynd4': {
        'name': 'CC BY-ND 4.0',
        'fullname': 'Attribution-NoDerivatives 4.0 International',
        'url': 'https://creativecommons.org/licenses/by-nd/4.0/'
    },
    'ccbync4': {
        'name': 'CC BY-NC 4.0',
        'fullname': 'Attribution-NonCommercial 4.0 International',
        'url': 'https://creativecommons.org/licenses/by-nc/4.0/'
    },
    'ccbyncsa4': {
        'name': 'CC BY-NC-SA 4.0',
        'fullname': 'Attribution-NonCommercial-ShareAlike 4.0 International',
        'url': 'https://creativecommons.org/licenses/by-nc-sa/4.0/'
    },
    'ccbyncnd4': {
        'name': 'CC BY-NC-ND 4.0',
        'fullname': 'Attribution-NonCommercial-NoDerivatives 4.0 International',
        'url': 'https://creativecommons.org/licenses/by-nc-nd/4.0/'
    },

    'ccby3': {
        'name': 'CC BY 3.0',
        'fullname': 'Attribution 3.0 International',
        'url': 'https://creativecommons.org/licenses/by/3.0/'
    },
    'ccbysa3': {
        'name': 'CC BY-SA 3.0',
        'fullname': 'Attribution-ShareAlike 3.0 International',
        'url': 'https://creativecommons.org/licenses/by-sa/3.0/'
    },

    'ccbynd3': {
        'name': 'CC BY-ND 3.0',
        'fullname': 'Attribution-NoDerivatives 3.0 International',
        'url': 'https://creativecommons.org/licenses/by-nd/3.0/'
    },
    'ccbync3': {
        'name': 'CC BY-NC 3.0',
        'fullname': 'Attribution-NonCommercial 3.0 International',
        'url': 'https://creativecommons.org/licenses/by-nc/3.0/'
    },
    'ccbyncsa3': {
        'name': 'CC BY-NC-SA 3.0',
        'fullname': 'Attribution-NonCommercial-ShareAlike 3.0 International',
        'url': 'https://creativecommons.org/licenses/by-nc-sa/3.0/'
    },
    'ccbyncnd3': {
        'name': 'CC BY-NC-ND 3.0',
        'fullname': 'Attribution-NonCommercial-NoDerivatives 3.0 International',
        'url': 'https://creativecommons.org/licenses/by-nc-nd/3.0/'
    },
    'cc01': {
        'name': 'CC0 1.0',
        'fullname': 'CC0 1.0 Universal - Public Domain Dedication',
        'url': 'https://creativecommons.org/publicdomain/zero/1.0/'
    },
    'publicdomain': {
        'name': 'Public Domain',
        'fullname': 'Public Domain Mark 1.0',
        'url': 'https://creativecommons.org/publicdomain/mark/1.0/'
    }
}

ANOTHER_READ_MORE_LINK = 'Continúa leyendo <span class="screen-reader-text">{title}</span>'
ANOTHER_READ_MORE_LINK_FORMAT = ' <a class="more-link" href="{url}#read_more_link">{text}</a>'

I18N_UNTRANSLATED_ARTICLES = 'remove'
I18N_UNTRANSLATED_PAGES = 'remove'
I18N_SUBSITES = {
    'de': {
        'ANOTHER_READ_MORE_LINK': 'Weiter lesen <span class="screen-reader-text">{title}</span>',
        'LOCALE': ('de_DE.UTF-8'),
    },
    'en': {
        'ANOTHER_READ_MORE_LINK': 'Keep reading <span class="screen-reader-text">{title}</span>',
        'LOCALE': ('en_US.UTF-8'),
        'MARKDOWN': {
            'extension_configs': {
                'markdown.extensions.codehilite': {'css_class': 'highlight'},
                'markdown.extensions.extra': {},
                'markdown.extensions.footnotes': {'BACKLINK_TITLE': 'Jump back to footnote %d in the text'},
                'markdown.extensions.meta': {},
            },
            'output_format': 'html5',
        }
    },
    'eo': {
        'ANOTHER_READ_MORE_LINK': 'Legu plu <span class="screen-reader-text">el {title}</span>',
        'LOCALE': ('eo.UTF-8'),
    },
    'fr': {
        'LOCALE': ('fr_FR.UTF-8'),
    },
}
JINJA_ENVIRONMENT = {'extensions': ['jinja2.ext.i18n']}
