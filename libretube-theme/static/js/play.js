// @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-v3-or-Later
const player = new Plyr('#player-ply', {
    captions: { active: true, language: 'es' },
    controls: [
        'play-large',
        'play',
        'progress',
        'current-time',
        'mute',
        'volume',
        'captions',
        'settings',
        'download',
        'fullscreen'
    ]
});
// @license-end
