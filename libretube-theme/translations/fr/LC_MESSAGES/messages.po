# French translations for PROJECT.
# Copyright (C) 2018 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2018.
#
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2019-11-23 21:04-0500\n"
"PO-Revision-Date: 2019-03-11 12:33-0500\n"
"Last-Translator: \n"
"Language: fr\n"
"Language-Team: fr <LL@li.org>\n"
"Plural-Forms: nplurals=2; plural=(n > 1)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.7.0\n"

#: templates/archives.html:5
#, python-format
msgid "Archivos de %(sitename)s"
msgstr "Archives de %(sitename)s"

#: templates/archives.html:9
#, python-format
msgid "Lista de artículos publicados en %(sitename)s"
msgstr "Liste des articles publiés dans %(sitename)s"

#: templates/archives.html:10
msgid "archivos,artículos,historia,lista de artículos"
msgstr "archives, articles, histoire, liste d'articles"

#: templates/archives.html:57
#, python-format
msgid "%(num)d artículo"
msgid_plural "%(num)d artículos"
msgstr[0] "%(num)d article"
msgstr[1] "%(num)d articles"

#: templates/article.html:90
msgid "Licencia"
msgstr "Licence"

#: templates/article.html:175
msgid "Publicado el"
msgstr "Posté le"

#: templates/article.html:178
#, python-format
msgid "Vídeos de %(nombre_mes)s de %(año)s"
msgstr "Vidéos de %(nombre_mes)s de %(año)s"

#: templates/article.html:181
msgid "de"
msgstr "de"

#: templates/article.html:183
#, python-format
msgid "Vídeos de %(año)s"
msgstr "Vidéos de %(año)s"

#: templates/article.html:197
#, python-format
msgid ""
"Lo siento, el sistema de comentarios no funciona sin JavaScript. Si "
"deseas, puedes %(abre_enlace)s enviar tu comentario por correo "
"electrónico %(cierra_enlace)s. El comentario será publicado en el espacio"
" reservado a comentarios de esta página."
msgstr ""
"Désolé, le système de commentaires ne fonctionne pas sans JavaScript. Si "
"vous le souhaitez, vous pouvez %(abre_enlace)s envoyer votre commentaire "
"par email %(cierra_enlace)s. Le commentaire sera publié dans "
"l'espaceréservé aux commentaires sur cette page."

#: templates/article.html:246 templates/base.html:113
#, python-format
msgid "Ver %(articulo)s"
msgstr "Regarder %(articulo)s"

#: templates/article_info.html:16 templates/article_info_aside.html:16
#, python-format
msgid "Artículos de %(nombre_mes)s de %(año)s"
msgstr "Articles de %(nombre_mes)s de %(año)s"

#: templates/article_info.html:20 templates/article_info_aside.html:20
#, python-format
msgid "Artículos de %(año)s"
msgstr "Articles de %(año)s"

#: templates/article_info.html:32
msgid "Fecha de modificación"
msgstr "Date de modification"

#: templates/article_info.html:47 templates/article_info_aside.html:34
msgid "Autor del artículo"
msgstr "Auteur de l'article"

#: templates/author.html:5 templates/author.html:11
#, python-format
msgid "Artículos escritos por %(author)s en %(sitename)s"
msgstr "Articles écrits par %(author)s dans %(sitename)s"

#: templates/author.html:12
#, python-format
msgid "articulista,artículos,author,autor %(author)s,escritor,%(author)s"
msgstr "article écrivain, articles, auteur, auteur %(author)s, écrivain,%(author)s"

#: templates/author.html:28
#, python-format
msgid "Autor: %(author)s"
msgstr "Auteur: %(author)s"

#: templates/authors.html:5
#, python-format
msgid "Créditos de %(sitename)s"
msgstr "Crédits de %(sitename)s"

#: templates/authors.html:9 templates/authors.html:14 templates/authors.html:20
#, python-format
msgid "Lista de autores de %(sitename)s"
msgstr "Liste des auteurs de %(sitename)s"

#: templates/authors.html:10
msgid "articulistas, autores, colaboradores, escritores, lista de autores"
msgstr "article écrivain, articles, auteur, liste des auteurs"

#: templates/authors.html:31
msgid "Artículos de vídeo"
msgstr "Articles vidéo"

#: templates/authors.html:36
#, python-format
msgid "Artículos escritos por %(author)s"
msgstr "Articles écrits par %(author)s"

#: templates/authors.html:42
msgid "Programación"
msgstr "Programmation"

#: templates/authors.html:44
msgid "Sitio para gente Libre"
msgstr "Site pour personnes libres"

#: templates/authors.html:45
msgid "Sitio web de Jesús E."
msgstr "Site web Jesus E."

#: templates/authors.html:48
msgid "Traducción"
msgstr "Traduction"

#: templates/authors.html:50
msgid "Sitio web de Alyssa Rosenzweig"
msgstr "Site Web d'Alyssa Rosenzweig"

#: templates/authors.html:54
msgid "Software que usa la página"
msgstr "Logiciel qui utilise la page"

#: templates/authors.html:61
msgid "Algunos programas de JavaScript"
msgstr "Quelques programmes JavaScript"

#: templates/authors.html:64
#, python-format
msgid "Sitio web de %(programa)s"
msgstr "Site web de %(programa)s"

#: templates/base.html:48
msgid "VideoTeca"
msgstr "VidéoThèque"

#: templates/base.html:66
msgid "Inicio"
msgstr "Initiation"

#: templates/base.html:70
msgid "Archivos"
msgstr "Les archives"

#: templates/base.html:93
msgid "Ver ahora"
msgstr "Voir maintenant"

#: templates/base.html:161 templates/base.html:163
msgid "Política de uso"
msgstr "Politique d'utilisation"

#: templates/base.html:168
msgid "Licencias de JavaScript"
msgstr "Licences JavaScript"

#: templates/base.html:172
msgid "Código fuente"
msgstr "Code source"

#: templates/categories.html:5 templates/categories.html:8
#, python-format
msgid "Lista de categorías de %(sitename)s"
msgstr "Liste des catégories de %(sitename)s"

#: templates/categories.html:9
msgid "categorías"
msgstr "catégories"

#: templates/categories.html:21
msgid "Lista de categorías"
msgstr "Liste de catégories"

#: templates/index.html:5
msgid "Página"
msgstr "Page"

#: templates/index.html:15
msgid "Videoteca de software libre brindando avance tecnológico"
msgstr "Vidéothèque de logiciels libres fournissant des avancées technologiques"

#: templates/period_archives.html:7 templates/period_archives.html:27
#, python-format
msgid "Archivos de %(año)d"
msgstr "Archives de %(año)d"

#: templates/period_archives.html:9 templates/period_archives.html:29
#, python-format
msgid "Archivos de %(mes)s de %(año)d"
msgstr "Fichiers de %(mes)s de %(año)d"

#: templates/period_archives.html:11 templates/period_archives.html:31
#, python-format
msgid "Archivos del %(dia)d de %(mes)s de %(año)d"
msgstr "Fichiers de %(dia)d sur %(mes)s sur %(año)d"

#: templates/tags.html:5 templates/tags.html:8
#, python-format
msgid "Lista de etiquetas de %(sitename)s"
msgstr "Liste des étiquettes de %(sitename)s"

#: templates/tags.html:9
msgid "etiquetas,palabras clave"
msgstr "tags, mots-clés"

#: templates/tags.html:16
msgid "Lista de etiquetas"
msgstr "Liste des étiquettes"
