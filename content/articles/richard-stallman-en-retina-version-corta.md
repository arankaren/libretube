Author: Jesús E.
Category: Noticias
Date: 2019-05-31 14:49:45
Image: 2019/05/rms-retina-version-corta.jpg
License: CC BY-ND 4.0
Slug: richard-stallman-en-retina-version-corta
Tags: richard stallman, libertad, libre, gnu
Title: Richard Stallman en Retina - version corta
Time: 2:19

<figure>
  <video id="player-ply" playsinline controls>
    <source src="https://archive.org/download/libreweb/Entrevista-Richard-Stallman-version-corta.webm" type="video/webm">
  </video>
  <figcaption>
    <h1>Richard Stallman en Retina - version corta</h1>
  </figcaption>
</figure>
