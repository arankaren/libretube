Author: Jorge Maldonado Ventura
Category: Cine
Date: 2017-07-16 12:15
Image: 2017/07/sin-parar.jpg
Lang: es
License: CC BY-SA 3.0
Modified: 2019-02-10 11:46:44
Og_video: https://archive.org/download/libreweb/sin-parar.webm
Slug: sin-parar
Tags: capitalismo, ciencia, cortometraje, cultura libre, educación, sistema, video
Time: 7:28
Title: Sin parar

<figure>
  <video id="player-ply" playsinline controls poster="{static}/wp-content/uploads/article/images/2017/07/video-poster-sin-parar.png">
    <source src="https://archive.org/download/libreweb/sin-parar.webm" type="video/webm">
  </video>
  <figcaption>
    <h1>Sin Parar</h1>
  </figcaption>
</figure>

«Desde que hace siglos apareciera el mayor invento creado por el hombre,
generación tras generación hay alguien encargado de revisarlo
minuciosamente [...] y que pueda seguir funcionando en todo el mundo sin
parar». Así comienza este cortometraje educativo sobre el sistema
capitalista. ¿Seguirá funcionando siempre **sin parar**?
