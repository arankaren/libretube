Author: VodafoneOne
Category: software libre
Date: 2016-12-28 15:52:58
Image: 2016/12/rms-el-mesias.jpg
License: CC BY-NC-ND 3.0
Slug: richard-stallman-el-mesias-del-software-libre
Tags: RMS
Time: 6:29
Title: Richard Stallman, el mesías del software libre

<figure>
  <video id="player-ply" playsinline controls>
    <source src="https://archive.org/download/libreweb/richard-stallman-el-mes%C3%ADas-del-software-libre.webm" type="video/webm">
  </video>
  <figcaption>
    <h1>Richard Stallman, el mesías del software libre</h1>
  </figcaption>
</figure>

Entrevista corta a [Richard M. Stallman][rms]{:target="_blank" rel="noopener noreferrer"}
en VodfoneOne.

Textos: José L. Álvarez Cedena

[rms]: https://es.wikipedia.org/wiki/Richard_Stallman
