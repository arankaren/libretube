Author: Jesús E.
Category: Cine
Date: 2018-09-25 11:05
Image: 2018/09/documental-snowden.jpg
License: CC BY-NC 3.0
Lang: es
Og_video: https://archive.org/download/libreweb/citizenfour-spanish.webm
Slug: citizenfour
Tags: snowden, citizenfour, libertad
Time: 1:48:40
Title: Citizenfour

<figure>
  <video id="player-ply" playsinline controls poster='{static}/wp-content/uploads/article/images/2018/09/video-poster-snowden.jpg'>
    <source src="https://archive.org/download/libreweb/citizenfour-spanish.webm" type="video/webm"/>
  </video>
  <figcaption>
    <h1>Citizenfour</h1>
  </figcaption>
</figure>

En enero de 2013, **Laura Poitras** comenzó a recibir correos electrónicos
cifrados firmados por un tal  **"Citizenfour"**

<details markdown="span">
<summary>Mostrar Más</summary>
[...] que le aseguraba tener pruebas de los programas de vigilancia
ilegales dirigidos por la NSA
en colaboración con otras agencias de inteligencia en todo el mundo.
Cinco meses más tarde, junto con los periodistas **Glenn Greenwald** y
**Ewen MacAskill** voló a **Hong Kong** para el primero de muchos encuentros
con un hombre anónimo que resultó ser **Edward Snowden**.
Para sus encuentros, viajó siempre con una cámara.
La película resultante es la historia que se desarrolla ante nuestros
ojos en este documental.
</details>
