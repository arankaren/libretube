Author: Jesús E.
Category: Tutorial
Date: 2019-11-08 12:56:11
Image: 2019/11/hyperbola-base.jpg
Slug: hyperbola-gnu-linux-libre-base
Tags: Hyperbola, GNU, Libre
Time: 47:37
Title: Hyperbola GNU+Linux-libre - BASE - Mode Legacy - MBR

<figure>
<video id="player-ply" playsinline controls>
  <source src="https://archive.org/download/hyperguide/base.webm" type="video/webm">
  <track kind="captions" label="English" src="{static}/wp-content/uploads/article/subtitles/2019/11/base.vtt" srclang="en">
</video>
<figcaption>
  <h1>Hyperbola GNU+Linux-libre - BASE - Mode Legacy - MBR</h1>
</figcaption>
</figure>

[Hyperbola][hyperbola]{:target='_blank' rel='noopener noreferrer'}
es un proyecto de [Software Libre][gnu]{:target='_blank' rel='noopener noreferrer'}
y [Cultura Libre][cultura-libre]{:target='_blank' rel='noopener noreferrer'} con el objetivo
de proporcionar un sistema operativo GNU/Linux totalmente libre llamado
Hyperbola GNU/Linux-libre.

[Guía escrita][guide]{:target='_blank' rel='noopener noreferrer'}

Subtitulado en inglés: Miguel Suarez

<details markdown="span">
<summary>Mostrar Más</summary>
Se basa en las instantáneas de Arch y el desarrollo
de Debian, con paquetes optimizados para CPU i686 y x86_64 bajo una forma de
Soporte a Largo Plazo (LTS). Hyperbola tiene como objetivo mantener sus
herramientas de paquete y administración simples, estables y seguras.
El objetivo principal es brindar al usuario el control completo de su sistema
con software 100% libre, cultura libre, seguridad, privacidad, estabilidad
e [init freedom][init-freedom]{:target='_blank' rel='noopener noreferrer'}.

El desarrollo se centra en un equilibrio de simplicidad, elegancia,
corrección de código y software libre de punta.

Su diseño ligero y simple hace que sea fácil extenderlo y moldearlo en
cualquier tipo de sistema que esté construyendo.

Puede encontrarnos en [irc][irc]{:target='_blank' rel='noopener noreferrer'},
[foros][foro]{:target='_blank' rel='noopener noreferrer'}
o [listas de correos][mail-list]{:target='_blank' rel='noopener noreferrer'}.
</details>

[guide]: https://conocimientoslibres.tuxfamily.org/guia-de-instalacion-de-hyperbola/
[cultura-libre]: https://freedomdefined.org/Definition
[foro]: https://forums.hyperbola.info/
[hyperbola]: https://www.hyperbola.info
[init-freedom]: https://www.devuan.org/os/init-freedom/
[gnu]: https://www.gnu.org/philosophy/free-sw.html
[irc]: irc://irc.freenode.net/#hyperbola
[mail-list]: https://lists.hyperbola.info/mailman/listinfo
