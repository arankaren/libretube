Author: Jesús E.
Category: courses
Date: 2019-03-25 11:09
Image: 2019/03/sintaxis-html5.jpg
Lang: es
Og_video: https://archive.org/download/coursehtml5/0003-Sintaxis-HTML5.webm
Slug: html5-sintaxis
Tags: html
Time: 9:57
Title: Sintaxis HTML5

<figure>
  <video id="player-ply" playsinline controls>
    <source src="https://archive.org/download/coursehtml5/0003-Sintaxis-HTML5.webm" type="video/webm">
    <track kind="captions" label="Español" src="{static}/wp-content/uploads/article/subtitles/2019/03/0003-sintaxis-html5-es.vtt" srclang="es" default>
  </video>
  <figcaption>
    <h1>Sintaxis HTML5 3/32</h1>
  </figcaption>
</figure>

En HTML5 se maneja una sintaxis simple sin tantos dolores de cabeza.
También coloco un enlace al [validador][validator]{:target="_blank" rel="noopener noreferrer"} de la W3C.
para testear el código :).
A continuación se muestra la sintaxis básica.

<details markdown="span">
<summary>Mostrar Más</summary>
Estructura básica:
~~~~{.html}
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Mi primera web en HTML5</title>
    </head>
    <body>
    </body>
</html>
~~~~
</details>

[validator]: https://validator.w3.org/
*[HTML5]: Hyper Text Markup Language version 5
*[W3C]: World Wide Web Consortium
