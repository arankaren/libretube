Author: Jesús E.
Category: courses
Date: 2019-03-25 11:06
Image: 2019/03/intro-html5.jpg
Lang: es
Og_video: https://archive.org/download/coursehtml5/0001-Introduccion.webm
Slug: html5-intro
Tags: html
Time: 9:16
Title: Introducción a HTML5

<figure>
  <video id="player-ply" playsinline controls>
    <source src="https://archive.org/download/coursehtml5/0001-Introduccion.webm" type="video/webm"/>
    <track kind="captions" label="Español" src="{static}/wp-content/uploads/article/subtitles/2019/03/introduccion-es.vtt" srclang="es" default>
  </video>
  <figcaption>
    <h1>Introducción a HTML5 1/32</h1>
  </figcaption>
</figure>

HTML tiene una nueva versión desde hace unos años, hablamos de la versión 5 del mismo.
Trayendo consigo nuevas etiquetas, las llamadas etiquetas semánticas.

<details markdown="span">
<summary>Mostrar Más</summary>
<ul class="ml-3">
<li>[Respositorio oficial][html]{:target="_blank" rel="noopener noreferrer"} de HTML</li>
<li>[w3schools][w3c]{:target="_blank" rel="noopener noreferrer"}</li>
<li>[Documentación][w3c.org]{:target="_blank" rel="noopener noreferrer"} de la W3C</li>
<li>[Validador][validator]{:target="_blank" rel="noopener noreferrer"}</li>
<li>[Editor de código][emacs-personal]{:target="_blank" rel="noopener noreferrer"}</li>
</ul>
</details>

[html]: https://github.com/whatwg/html
[w3c]: https://www.w3schools.com/html/default.asp
[w3c.org]: https://www.w3.org/TR/html/
[validator]: https://validator.w3.org/
[emacs-personal]: https://libregit.org/heckyel/emacs-personal.git

*[HTML]: Hyper Text Markup Language
*[W3C]: World Wide Web Consortium
