Author: Jesús E.
Category: courses
Date: 2019-04-09 12:23:54
Image: 2019/04/etiqueta-article.jpg
Og_video: https://archive.org/download/coursehtml5/0013-article.webm
Slug: etiqueta-article
Tags: html
Time: 3:43
Title: Etiqueta article

<figure>
  <video id="player-ply" playsinline controls>
    <source src="https://archive.org/download/coursehtml5/0013-article.webm" type="video/webm">
    <track kind="captions" label="Español" src="{static}/wp-content/uploads/article/subtitles/2019/04/0013-article-es.vtt" srclang="es" default>
  </video>
  <figcaption>
    <h1>Etiqueta article 13/32</h1>
  </figcaption>
</figure>

Representa una sección de una página que consta de una composición que
forma parte independiente de un documento, página o sitio.

<details markdown="span">
<summary>Mostrar Más</summary>
<p class="mb-0">Más sobre la etiqueta `<article>` en la [w3schools][w3-article]{:target="_blank" rel="noopener noreferrer"}</p>
<p class="mb-0">[Validador][validator]{:target="_blank" rel="noopener noreferrer"} de la W3C</p>
</details>

[w3-article]: https://www.w3schools.com/tags/tag_article.asp
[validator]: https://validator.w3.org/

*[W3C]: World Wide Web Consortium
