Author: Jesús E.
Category: courses
Date: 2019-03-25 12:11
Image: 2019/03/codificacion-html5.jpg
Lang: es
Og_video: https://archive.org/download/coursehtml5/0004-Codificacion.webm
Slug: html5-codificacion
Tags: html
Time: 16:28
Title: Codificación HTML5

<figure>
  <video id="player-ply" playsinline controls>
    <source src="https://archive.org/download/coursehtml5/0004-Codificacion.webm" type="video/webm">
    <track kind="captions" label="Español" src="{static}/wp-content/uploads/article/subtitles/2019/03/0004-codificacion-es.vtt" srclang="es" default>
  </video>
  <figcaption>
    <h1>Codificación HTML5 4/32</h1>
  </figcaption>
</figure>

<details markdown="span">
<summary>Mostrar Más</summary>
Código de ayuda:
~~~~{.html}
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
~~~~
[Validador][validator]{:target="_blank" rel="noopener noreferrer"} de la W3C.
</details>

[validator]: https://validator.w3.org/

*[W3C]: World Wide Web Consortium
