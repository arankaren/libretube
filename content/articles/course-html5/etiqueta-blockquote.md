Author: Jesús E.
Category: course
Date: 2019-06-07 16:38:57
Image: 2019/06/etiqueta-blockquote.jpg
Slug: etiqueta-blockquote
Tags: HTML, html, blockquote
Title: Etiqueta blockquote
Time: 7:02

<figure>
  <video id="player-ply" playsinline controls>
    <source src="https://archive.org/download/coursehtml5/0020-blockquote.webm" type="video/webm">
  </video>
  <figcaption>
    <h1>Etiqueta blockquote 20/32</h1>
  </figcaption>
</figure>
