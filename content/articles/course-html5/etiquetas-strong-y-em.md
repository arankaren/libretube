Author: Jesús E.
Category: course
Date: 2019-04-09 11:30:00
Image: 2019/04/etiquetas-strong-em.jpg
Og_video: https://archive.org/download/coursehtml5/0011-Etiquetas-strong-em.webm
Slug: etiquetas-strong-em
Tags: hml
Time: 2:16
Title: Etiquetas strong y em

<figure>
  <video id="player-ply" playsinline controls>
    <source src="https://archive.org/download/coursehtml5/0011-Etiquetas-strong-em.webm" type="video/webm">
    <track kind="captions" label="Español" src="{static}/wp-content/uploads/article/subtitles/2019/04/0011-etiquetas-strong-em-es.vtt" srclang="es" default>
  </video>
  <figcaption>
    <h1>Etiquetas strong y em 11/32</h1>
  </figcaption>
</figure>

La etiqueta `strong` brinda una gran importancia a un parráfo, una palabra o una frase y
`em` representa el énfasis del estrés de sus contenidos.

<details markdown="span">
<summary>Mostrar Más</summary>
<p class="mb-0">Más sobre la etiqueta `<strong>` en la [w3schools][w3-strong]{:target="_blank" rel="noopener noreferrer"}</p>
<p class="mb-0">Más sobre la etiqueta `<em>` en la [w3schools][w3-em]{:target="_blank" rel="noopener noreferrer"}</p>
<p class="mb-0">[Validador][validator]{:target="_blank" rel="noopener noreferrer"} de la W3C</p>
</details>

[w3-strong]: https://www.w3schools.com/tags/tag_strong.asp
[w3-em]: https://www.w3schools.com/tags/tag_em.asp
[validator]: https://validator.w3.org/

*[W3C]: World Wide Web Consortium
