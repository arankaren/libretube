Author: Jesús E.
Category: courses
Date: 2019-03-25 18:44
Image: 2019/03/nav-ul-ol-li.jpg
Og_video: https://archive.org/download/coursehtml5/0008-nav-ul-ol-li.webm
Slug: etiqueta-nav-ul-ol-li
Tags: html
Time: 8:20
Title: Etiqueta nav ul ol li

<figure>
  <video id="player-ply" playsinline controls>
    <source src="https://archive.org/download/coursehtml5/0008-nav-ul-ol-li.webm" type="video/webm">
    <track kind="captions" label="Español" src="{static}/wp-content/uploads/article/subtitles/2019/03/0008-nav-ul-ol-li-es.vtt" srclang="es" default>
  </video>
  <figcaption>
    <h1>Etiqueta nav ul ol li 8/32</h1>
  </figcaption>
</figure>

- El elemento de navegación «nav» es una sección que contiene enlaces a otros documentos o partes del documento actual.
- El elemento ul representa una lista de elementos, donde el orden de los elementos no es importante.
- El elemento ol representa una lista de artículos, donde los artículos han sido ordenados intencionalmente.
- El elemento li representa un elemento de lista.

<details markdown="span">
<summary>Mostrar Más</summary>
<p class="mb-0">Más sobre la etiqueta `<nav>` en la [w3schools][w3-nav]{:target="_blank" rel="noopener noreferrer"}</p>
<p class="mb-0">Más sobre la etiqueta `<ul>` en la [w3schools][w3-ul]{:target="_blank" rel="noopener noreferrer"}</p>
<p class="mb-0">Más sobre la etiqueta `<ol>` en la [w3schools][w3-ol]{:target="_blank" rel="noopener noreferrer"}</p>
<p class="mb-0">Más sobre la etiqueta `<li>` en la [w3schools][w3-li]{:target="_blank" rel="noopener noreferrer"}</p>
<p class="mb-0">[Validador][validator]{:target="_blank" rel="noopener noreferrer"} de la W3C</p>
</details>

[w3-nav]: https://www.w3schools.com/tags/tag_nav.asp
[w3-ul]: https://www.w3schools.com/tags/tag_ul.asp
[w3-ol]: https://www.w3schools.com/tags/tag_ol.asp
[w3-li]: https://www.w3schools.com/tags/tag_li.asp
[validator]: https://validator.w3.org/

*[W3C]: World Wide Web Consortium
