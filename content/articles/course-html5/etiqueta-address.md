Author: Jesús E.
Category: course
Date: 2019-06-06 12:42:10
Image: 2019/06/etiqueta-address.jpg
Slug: etiqueta-address
Tags: HTML, html, etiqueta address
Title: Etiqueta address
Time: 6:25

<figure>
  <video id="player-ply" playsinline controls>
    <source src="https://archive.org/download/coursehtml5/0019-address.webm" type="video/webm">
  </video>
  <figcaption>
    <h1>Etiqueta address 19/32</h1>
  </figcaption>
</figure>
