Author: Jesús E.
Category: course
Date: 2019-06-07 16:55:36
Image: 2019/06/etiquetas-cite-y-q.jpg
Slug: etiquetas-cite-y-q
Tags: HTML, html, cite, q
Title: Etiquetas cite y q
Time: 2:56

<figure>
  <video id="player-ply" playsinline controls>
    <source src="https://archive.org/download/coursehtml5/0021-cite-y-q.webm" type="video/webm">
  </video>
  <figcaption>
    <h1>Etiquetas cite y q 21/32</h1>
  </figcaption>
</figure>
