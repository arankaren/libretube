Author: Jesús E.
Category: course
Date: 2019-06-06 12:25:11
Image: 2019/06/etiqueta-main.jpg
Slug: etiqueta-main
Tags: HTML. html, etiqueta main
Title: Etiqueta main
Time: 3:52

<figure>
  <video id="player-ply" playsinline controls>
    <source src="https://archive.org/download/coursehtml5/0018-main.webm" type="video/webm">
  </video>
  <figcaption>
    <h1>Etiqueta main 18/32</h1>
  </figcaption>
</figure>
