Author: Jesús E.
Category: course
Date: 2019-05-09 12:24:54
Image: 2019/05/etiqueta-div.jpg
Slug: etiqueta-div
Tags: HTML
Time: 3:56
Title: Etiqueta div

<figure>
  <video id="player-ply" playsinline controls>
    <source src="https://archive.org/download/coursehtml5/0015-div.webm" type="video/webm">
  </video>
  <figcaption>
    <h1>Etiqueta div 15/32</h1>
  </figcaption>
</figure>
