Author: Jesús E.
Category: courses
Date: 2019-06-03 15:13:07
Image: 2019/06/etiqueta-figure.jpg
Slug: etiqueta-figure
Tags: HTML, html, figure, html5
Title: Etiqueta figure
Time: 6:19

<figure>
  <video id="player-ply" playsinline controls>
    <source src="https://archive.org/download/coursehtml5/0017-figure.webm" type="video/webm">
  </video>
  <figcaption>
    <h1>Etiqueta figure 17/32</h1>
  </figcaption>
</figure>
