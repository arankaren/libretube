Author: Jesús E.
Category: courses
Date: 2019-03-25 11:07
Image: 2019/03/nuevas-etiquetas.jpg
Og_video: https://archive.org/download/coursehtml5/0002-nuevas-etiquetas.webm
Slug: html5-etiquetas
Tags: html
Time: 2:36
Title: Nuevas etiquetas de HTML5

<figure>
  <video id="player-ply" playsinline controls>
    <source src="https://archive.org/download/coursehtml5/0002-nuevas-etiquetas.webm" type="video/webm"/>
    <track kind="captions" src="{static}/wp-content/uploads/article/subtitles/2019/03/0002-nuevas-etiquetas-es.vtt" srclang="es" label="Español" default>
  </video>
  <figcaption>
    <h1>Nuevas etiquetas de HTML5 2/32</h1>
  </figcaption>
</figure>

Las nuevas etiquetas de HTML5 ahora son semánticas, es decir que dan
sentido a la información escrita en una página web.
Podéis revisar en los enlaces de abajo o consultar en
[mozilla developers][mozilla-html5]{:target="_blank" rel="noopener noreferrer"}.

<details markdown="span">
<summary>Mostrar Más</summary>
<p class="mb-0">Más información sobre HTML5 en [w3schools][w3c-html5]{:target="_blank" rel="noopener noreferrer"}.</p>
<p class="mb-0">Un [Validador][validator]{:target="_blank" rel="noopener noreferrer"} de la W3C</p>
</details>

[w3c-html5]: https://www.w3schools.com/html/html5_intro.asp
[mozilla-html5]: https://developer.mozilla.org/es/docs/HTML/HTML5/HTML5_lista_elementos
[validator]: https://validator.w3.org/
*[HTML5]: Hyper Text Markup Language version 5
*[W3C]: World Wide Web Consortium
