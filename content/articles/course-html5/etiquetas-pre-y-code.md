Author: Jesús E.
Category: course
Date: 2019-06-07 17:14:15
Image: 2019/06/etiquetas-pre-y-code.jpg
Slug: etiquetas-pre-y-code
Tags: HTML, html, pre, code
Title: Etiquetas pre y code
Time: 6:04

<figure>
  <video id="player-ply" playsinline controls>
    <source src="https://archive.org/download/coursehtml5/0024-pre-y-code.webm" type="video/webm">
  </video>
  <figcaption>
    <h1>Etiquetas pre y code 24/32</h1>
  </figcaption>
</figure>
