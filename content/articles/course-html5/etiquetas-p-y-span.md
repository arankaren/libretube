Author: Jesús E.
Category: courses
Date: 2019-04-09 11:12:18
Image: 2019/04/etiquetas-span-p.jpg
Og_video: https://archive.org/download/coursehtml5/0010-Etiquetas-p-span.webm
Slug: etiquetas-p-span
Tags: html
Time: 3:14
Title: etiquetas p y span

<figure>
  <video id="player-ply" playsinline controls>
    <source src="https://archive.org/download/coursehtml5/0010-Etiquetas-p-span.webm" type="video/webm">
    <track kind="captions" label="Español" src="{static}/wp-content/uploads/article/subtitles/2019/04/0010-etiquetas-p-span-es.vtt" srclang="es" default>
  </video>
  <figcaption>
    <h1>Etiquetas p y span 10/32</h1>
  </figcaption>
</figure>

El elemento `p` representa un párrafo y el elemento `span` no significa nada por sí solo,
pero puede ser útil cuando se usa junto con los atributos globales,
por ejemplo: class, lang o dir.

<details markdown="span">
<summary>Mostrar Más</summary>
<p class="mb-0">Más sobre la etiqueta `<p>` en la [w3schools][w3-p]{:target="_blank" rel="noopener noreferrer"}</p>
<p class="mb-0">Más sobre la etiqueta `<span>` en la [w3schools][w3-span]{:target="_blank" rel="noopener noreferrer"}</p>
<p class="mb-0">[Validador][validator]{:target="_blank" rel="noopener noreferrer"} de la W3C</p>
</details>

[w3-p]: https://www.w3schools.com/tags/tag_p.asp
[w3-span]: https://www.w3schools.com/tags/tag_span.asp
[validator]: https://validator.w3.org/

*[W3C]: World Wide Web Consortium
