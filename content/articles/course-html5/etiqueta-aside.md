Author: Jesús E.
Category: courses
Date: 2019-04-10 13:10:01
Image: 2019/04/aside-html5.jpg
Og_video: https://archive.org/download/coursehtml5/0014-aside.webm
Slug: etiqueta-aside
Tags: html5
Time: 5:55
Title: Etiqueta aside

<figure>
  <video id="player-ply" playsinline controls>
    <source src="https://archive.org/download/coursehtml5/0014-aside.webm" type="video/webm">
    <track kind="captions" label="Español" src="{static}/wp-content/uploads/article/subtitles/2019/04/0014-aside-es.vtt" srclang="es" default>
  </video>
  <figcaption>
    <h1>Etiqueta aside 14/32</h1>
  </figcaption>
</figure>

Representa una sección de una página que consiste en una información que está relacionado tangencialmente
con la información de alrededor del elemento aparte, y que podría considerarse separado de la información principal.

<details markdown="span">
<summary>Mostrar Más</summary>
<p class="mb-0">Más sobre la etiqueta `<aside>` en la [w3schools][w3-aside]{:target="_blank" rel="noopener noreferrer"}</p>
<p class="mb-0">[Validador][validator]{:target="_blank" rel="noopener noreferrer"} de la W3C</p>
</details>

[w3-aside]: https://www.w3schools.com/tags/tag_aside.asp
[validator]: https://validator.w3.org/

*[W3C]: World Wide Web Consortium
