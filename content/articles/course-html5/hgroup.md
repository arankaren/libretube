Author: Jesús E.
Category: courses
Date: 2019-03-25 18:16
Image: 2019/03/hgroup-html5.jpg
Og_video: https://archive.org/download/coursehtml5/0007-hgroup.webm
Slug: html5-hgroup
Tags: html
Time: 3:19
Title: hgroup

<figure>
  <video id="player-ply" playsinline controls>
    <source src="https://archive.org/download/coursehtml5/0007-hgroup.webm" type="video/webm">
    <track kind="captions" label="Español" src="{static}/wp-content/uploads/article/subtitles/2019/03/0007-hgroup-es.vtt" srclang="es" default>
  </video>
  <figcaption>
    <h1>Hgroup HTML5 7/32</h1>
  </figcaption>
</figure>

Hgroup fue una etiqueta que apareció en HTML5, su función es agrupar las
etiquetas de títulos las `h`. Pero desde las versión 5.1 de HTML ya no se
recomienda debido a que tener demasiadas `h` de un mismo tipo traen problemas
de SEO.

<details markdown="span">
<summary>Mostrar Más</summary>
<p class="mb-0">Etiqueta hgroup en desuso.</p>
<p class="mb-0">Este elemento fue removido de la especificacion HTML5 por la W3C, por favor no usar más.</p>
<p class="mb-0">Referencia en [mozilla developers][mozilla-web]{:target="_blank" rel="noopener noreferrer"}</p>
<p class="mb-0">[Validador][validator]{:target="_blank" rel="noopener noreferrer"} de la W3C</p>
</details>

[mozilla-web]: https://developer.mozilla.org/es/docs/Web/HTML/Elemento/hgroup
[validator]: https://validator.w3.org/

*[HTML]: Hyper Text Markup Language
*[HTML5]: Hyper Text Markup Language version 5
*[SEO]: Search Engine Optimization
*[W3C]: World Wide Web Consortium
