Author: Jesús E.
Category: courses
Date: 2019-04-09 10:40
Image: 2019/04/etiqueta-a.jpg
Og_video: https://archive.org/download/coursehtml5/0009-Etiqueta-a.webm
Slug: etiqueta-a
Tags: html5,video
Time: 3:23
Title: Etiqueta a

<figure>
  <video id="player-ply" playsinline controls>
    <source src="https://archive.org/download/coursehtml5/0009-Etiqueta-a.webm" type="video/webm">
    <track kind="captions" label="Español" src="{static}/wp-content/uploads/article/subtitles/2019/04/0009-etiqueta-a-es.vtt" srclang="es" default>
  </video>
  <figcaption>
    <h1>Etiqueta a 9/32</h1>
  </figcaption>
</figure>

Si el elemento `a` tiene un atributo href, entonces representa un hipervínculo (un ancla de hipertexto).

<details markdown="span">
<summary>Mostrar Más</summary>
<p class="mb-0">Más sobre la etiqueta `<a>` en la [w3schools][w3school]{:target="_blank" rel="noopener noreferrer"}</p>
<p class="mb-0">[Validador][validator]{:target="_blank" rel="noopener noreferrer"} de la W3C</p>
</details>

[w3school]: https://www.w3schools.com/tags/tag_a.asp
[validator]: https://validator.w3.org/

*[W3C]: World Wide Web Consortium
