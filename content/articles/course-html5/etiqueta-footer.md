Author: Jesús E.
Category: course
Date: 2019-05-31 14:14:53
Image: 2019/05/etiqueta-footer.jpg
Slug: etiqueta-footer
Tags: html5
Title: Etiqueta footer
Time: 6:28

<figure>
  <video id="player-ply" playsinline controls>
    <source src="https://archive.org/download/coursehtml5/0016-footer.webm" type="video/webm">
  </video>
  <figcaption>
    <h1>Etiqueta footer 16/32</h1>
  </figcaption>
</figure>
