Author: Jesús E.
Category: courses
Date: 2019-03-25 17:59
Image: 2019/03/titulos-html5.jpg
Og_video: https://archive.org/download/coursehtml5/0006-h1-h2-h3-h4-h5-h6.webm
Slug: html5-titulos
Tags: html
Time: 7:55
Title: Títulos en HTML5

<figure>
  <video id="player-ply" playsinline controls>
     <source src="https://archive.org/download/coursehtml5/0006-h1-h2-h3-h4-h5-h6.webm" type="video/webm">
     <track kind="captions" label="Español" src="{static}/wp-content/uploads/article/subtitles/2019/03/0006-titulos-es.vtt" srclang="es" default>
  </video>
  <figcaption>
    <h1>Títulos en HTML5 6/32</h1>
  </figcaption>
</figure>

En el vídeo se explica de la importancia de las etiquetas
de título o también llamadas encabezados por su nombre en inglés
**heading**

<details markdown="span">
<summary>Mostrar más</summary>
<p class="mb-0">Más información sobre las etiquetas `h` en [w3schools][titles]{:target="_blank" rel="noopener noreferrer"}.</p>
<p class="mb-0">Un [Validador][validator]{:target="_blank" rel="noopener noreferrer"} de la W3C</p>
</details>

[titles]: https://www.w3schools.com/hTml/html_headings.asp
[validator]: https://validator.w3.org/
*[W3C]: World Wide Web Consortium
