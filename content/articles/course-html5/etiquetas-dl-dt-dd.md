Author: Jesús E.
Category: course
Date: 2019-06-07 17:02:47
Image: 2019/06/etiquetas-dl-dt-dd.jpg
Slug: etiquetas-dl-dt-dd
Tags: HTML, html, dl, dt, dd
Title: Etiquetas dl, dt y dd
Time: 7:36

<figure>
  <video id="player-ply" playsinline controls>
    <source src="https://archive.org/download/coursehtml5/0022-dl-dt-dd.webm" type="video/webm">
  </video>
  <figcaption>
    <h1>Etiquetas dl, dt y dd 22/32</h1>
  </figcaption>
</figure>
