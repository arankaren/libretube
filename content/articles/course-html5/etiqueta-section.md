Author: Jesús E.
Category: courses
Date: 2019-04-09 12:11:46
Image: 2019/04/etiqueta-section.jpg
Og_video: https://archive.org/download/coursehtml5/0012-section.webm
Slug: etiqueta-section
Tags: html
Time: 2:42
Title: Etiqueta section

<figure>
  <video id="player-ply" playsinline controls>
    <source src="https://archive.org/download/coursehtml5/0012-section.webm" type="video/webm">
    <track kind="captions" label="Español" src="{static}/wp-content/uploads/article/subtitles/2019/04/0012-section-es.vtt" srclang="es" default>
  </video>
  <figcaption>
    <h1>Etiqueta section 12/32</h1>
  </figcaption>
</figure>

Representa un documento genérico o sección de aplicación. En este contexto, una sección es una agrupación
temática de la información, normalmente con un encabezado, posiblemente con un pie de página.

<details markdown="span">
<summary>Mostrar Más</summary>
<p class="mb-0">Más sobre la etiqueta `<section>` en la [w3schools][w3-section]{:target="_blank" rel="noopener noreferrer"}</p>
<p class="mb-0">[Validador][validator]{:target="_blank" rel="noopener noreferrer"} de la W3C</p>
</details>

[w3-section]: https://www.w3schools.com/tags/tag_section.asp
[validator]: https://validator.w3.org/

*[W3C]: World Wide Web Consortium
