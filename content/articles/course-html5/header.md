Author: Jesús E.
Category: courses
Date: 2019-03-25 12:12
Image: 2019/03/header-html5.jpg
Lang: es
Og_video: https://archive.org/download/coursehtml5/0005-header.webm
Slug: html5-header
Tags: html
Time: 5:37
Title: Header

<figure>
  <video id="player-ply" playsinline controls>
  <source src="https://archive.org/download/coursehtml5/0005-header.webm" type="video/webm">
    <track kind="captions" label="Español" src="{static}/wp-content/uploads/article/subtitles/2019/03/0005-header-es.vtt" srclang="es" default>
  </video>
  <figcaption>
    <h1>Header HTML5 5/32</h1>
  </figcaption>
</figure>

Header representa el "encabezado" de un documento o sección de un documento.

<details markdown="span">
<summary>Mostrar Más</summary>
<p class="mb-0">Más información sobre la etiqueta `<header>` en [w3schools][header]{:target="_blank" rel="noopener noreferrer"}.</p>
<p class="mb-0">[Validador][validator]{:target="_blank" rel="noopener noreferrer"} de la W3C</p>
</details>

[header]: https://www.w3schools.com/tags/tag_header.asp
[validator]: https://validator.w3.org/

*[W3C]: World Wide Web Consortium
