Author: PMPC
Category: Cine
Date: 2019-06-15 12:04:39
Image: 2019/06/pmpc.jpg
Slug: public-money-public-code
Tags: public money
Title: Public Money, Public Code
Time: 3:47

<figure>
  <video id="player-ply" playsinline controls>
    <source data-res="1080" src="https://download.fsfe.org/videos/pmpc/pmpc_es_1080p_3300_a32.webm" type="video/webm">
    <source data-res="360" src="https://download.fsfe.org/videos/pmpc/pmpc_es_mobile.webm" type="video/webm">
  </video>
  <figcaption>
    <h1>Public Money, Public Code</h1>
  </figcaption>
</figure>

¿Por qué el software creado con dinero de los impuestos no se publica como Software Libre?
Queremos una legislación que permita que el software desarrollado para el sector público
y financiado con recursos públicos esté disponible públicamente bajo una licencia de
Software Libre y Código Abierto. Si es dinero público debería ser también código público.

¡El código pagado por los ciudadanos debería estar disponible para los ciudadanos!

<https://publiccode.eu/es/>{:target="_blank" rel="noopener noreferrer"}
