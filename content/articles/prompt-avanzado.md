Author: Jesús E.
Category: GNU/Linux
Date: 2017-12-05 11:34
Modified: 2019-02-06 11:34
Image: 2017/12/prompt-avanzado.jpg
Og_video: https://archive.org/download/libreweb/0001-15599.webm
Slug: prompt-avanzado
Tags: bash, hyperbash, shell
Time: 8:39
Title: Prompt avanzado

<figure>
  <video id="player-ply" playsinline controls>
    <source src="https://archive.org/download/libreweb/0001-15599.webm" type="video/webm">
  </video>
  <figcaption>
    <h1>Prompt avanzado</h1>
  </figcaption>
</figure>

Mayormente necesitamos del [intérprete de comandos][bash]{:target="_blank" rel="noopener noreferrer"}
para realizar una que otra tarea, quizás resulte tedioso armar
nuestra propia configuración de `.bashrc`.

<details markdown="span">
<summary>Mostrar Más</summary>
Pero como en la [World Wide Web][www]{:target="_blank" rel="noopener noreferrer"}
existe mucha información útil, se ha logrado escribir una
configuración prudente del ya mencionado `.bashrc` para distros
basadas en Arch como [Hyperbola][hypersite]{:target="_blank" rel="noopener noreferrer"} o
[Parabola][parasite]{:target="_blank" rel="noopener noreferrer"} , en efecto estas
2 últimas son [distros 100 % Libres][freedistros]{:target="_blank" rel="noopener noreferrer"}.

¿Y dónde consigo una copia?, sencillo puedes descargarlo desde
[notabug][notabug]{:target="_blank" rel="noopener noreferrer"}
bajo la Licencia [GPLv3][license]{:target="_blank" rel="noopener noreferrer"}.

</details>

[bash]: https://es.wikipedia.org/wiki/Bash
[www]: https://es.wikipedia.org/wiki/World_Wide_Web
[hypersite]: https://www.hyperbola.info/
[parasite]: https://www.parabola.nu/
[freedistros]: https://www.gnu.org/distros/free-distros.es.html
[notabug]: https://notabug.org/heckyel/hyperbash
[license]: https://www.gnu.org/licenses/gpl-3.0.html
