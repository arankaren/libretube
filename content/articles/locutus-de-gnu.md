Author: Jorge Maldonado Ventura
Category: Cine
Date: 2017-02-01 00:03
Image: 2017/02/locutus-gnu.jpg
Lang: es
License: Public Domain
Modified: 2019-02-10 12:23
Og_video: https://archive.org/download/libreweb/locutus.webm
Slug: locutus-de-gnu
Status: published
Tags: GNU/Linux, gracioso, humor, software libre, software privativo, Star Trek, video
Time: 2:48
Title: Locutus de GNU

<figure>
  <video id="player-ply" playsinline controls
         poster="{static}/wp-content/uploads/article/images/2017/02/video-poster-locutus.png">
    <source src="https://archive.org/download/libreweb/locutus.webm" type="video/webm">
  </video>
  <figcaption>
    <h1>Locutus de GNU</h1>
  </figcaption>
</figure>

Encontré [esta parodia][locutus]{:target="_blank" rel="noopener noreferrer"}
muy graciosa. Para entenderla al menos debéis saber qué son el software
libre y el software privativo. Espero que os guste.

[locutus]: https://goblinrefuge.com/mediagoblin/u/locutus/m/locutus-de-gnu/
