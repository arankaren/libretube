Author: Jesús E.
Category: Opinión
Date: 2018-06-09 10:07
Image: 2018/06/rms-dominio-digital.jpg
License: CC BY-ND 4.0
Og_video: https://archive.org/download/libreweb/StallmanenDominioDigital.webm
Slug: rms-en-dominio-digital
Tags: GNU/Linux
Time: 56:07
Title: RMS en Dominio digital

<figure>
  <video id="player-ply" playsinline controls poster='{static}/wp-content/uploads/article/images/2018/06/video-poster-rms.jpg'>
    <source src="https://archive.org/download/libreweb/StallmanenDominioDigital.webm" type="video/webm"/>
  </video>
  <figcaption>
    <h1>RMS en Dominio digital</h1>
  </figcaption>
</figure>

El pasado 30 de mayo, [Richard M. Stallman][rms]{:target="_blank" rel="noopener noreferrer"}
se presentó en Argentina en el programa radial Dominio Digital.
Dado que hay bastantes puntos importantes que se tomaron
en cuenta durante el programa aquí os dejamos una copia de
la Entrevista en vídeo.

<details markdown="span">
<summary>Mostrar Más</summary>
**Dominio Digital** es un programa sobre informática, emitido en Televisión
Argentina entre 1996 y 2011.En el año 2018 volvió en formato de radio.

Sus integrantes son **Claudio Regis** (Conductor) y **Daniela Gastaminza**,
**Alejandro Ponike** y **Daniel "Chacal" Sentinelli** (columnistas).
</details>

[rms]: https://es.wikipedia.org/wiki/Richard_Stallman
