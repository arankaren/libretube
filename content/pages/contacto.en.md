Author: Jesús E.
Date: 2019-02-04 07:51
Lang: en
Modified: 2019-02-04 07:52
Slug: contacto
Status: published
Title: Contact

My email is
[heckyel@hyperbola.info](mailto:heckyel@hyperbola.info). Use my
<abbr title="GNU Privacy Guard">GPG</abbr> public key
([4DF2 1B6A 7C10 21B2 5C36 0914 F6EE 7BC5 9A31 5766]({static}/heckyel_pub.asc))
so that other people cannot read the message.
