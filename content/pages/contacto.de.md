Author: Jesús E.
Date: 2019-02-04 07:52
Lang: de
Modified: 2019-02-04 07:55
Slug: contacto
Status: published
Title: Kontakt

Mein Email ist
[heckyel@hyperbola.info](mailto:heckyel@hyperbola.info). Nutz
meinen
<abbr title="GNU Privacy Guard">GPG</abbr> öffentlichen Schlüssel
([4DF2 1B6A 7C10 21B2 5C36 0914 F6EE 7BC5 9A31 5766]({static}/heckyel_pub.asc)),
damit die Nachricht nicht von anderen Personen gelesen werden kann.
