Author: Jesús E.
Date: 2018-03-05 16:30
Modified: 2018-07-28 15:50
Lang: es
Slug: política-de-uso
Status: hidden
Title: Política de uso

Se aceptan sugerencias y cambios a estas políticas enviados mediante el
[gestor de incidencias de LibreTube](https://libregit.org/heckyel/libretube/issues)
o usando
nuestro [correo de contacto]({filename}/pages/contacto.md).

## Condiciones de uso

No nos hacemos responsables sobre los posibles problemas derivados del
uso de nuestro sitio web. Aceptamos críticas y correcciones para mejorar
la página y subsanar los posibles errores que hayamos podido cometer.

LibreTube respeta las opiniones, críticas o sugerencias expresadas en
comentarios. Nos reservamos el derecho de eliminar mensajes
publicitarios.

En la medida de lo posible, tratamos de hacer el sitio web accesible al
mayor número de personas posible: personas discapacitadas, a quien
navega con JavaScript desactivado, personas que usan navegadores de
texto, con poco ancho de banda, etc.

Donde no se indique lo contrario, las licencias de LibreTube son la de
dominio público
[CC0](https://creativecommons.org/publicdomain/mark/1.0/), para el
<span title="textos, imágenes, vídeos, comentarios...">contenido</span>,
y la de software libre
<abbr title="Affero General Public License, version 3">AGPLv3</abbr>,
para el software. LibreTube también usa software
producido por terceros que puede encontrar bajo otra licencia de
software libre, consulte [Información de
licencias](https://libregit.org/heckyel/libretube#informaci%C3%B3n-de-licencias)
para más información sobre las licencias de software.

## Política de privacidad

LT está alojado en [TuxFamily](https://tuxfamily.org). Este
proveedor de alojamiento web utiliza un servidor
[Nginx](https://es.wikipedia.org/wiki/Nginx) que almacena
en ficheros datos de acceso: dirección
<abbr title="Internet Protocol">IP</abbr>, navegador utilizado, fecha de
visita, etc. Esta información no es personal, pero podría ser
relacionada con la persona visitante. El sitio web puede ser accedido
usando un proxy como
[Tor](https://es.wikipedia.org/wiki/Tor_(red_de_anonimato)) o una
<a href="https://es.wikipedia.org/wiki/Red_privada_virtual"><abbr title="Red Privada Virtual">RPV</abbr></a>
para contar con mayor privacidad.

Sobre los artículos se pueden realizar comentarios anónimos, con nombres
falsos o con datos reales. En caso de que alguien desee eliminar o
rectificar un comentario, deberá
[contactarnos]({filename}/pages/contacto.md) demostrándonos
que escribió ese comentario, en cuyo caso indicaremos que el comentario
ha sido eliminado o modificado de la forma que estimemos oportuna,
siempre intentando ser lo más transparentes posibles con el resto de
participantes en la conversación y evitando perder el contexto de otros
comentarios.
