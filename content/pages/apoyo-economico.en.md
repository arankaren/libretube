Author: Jesús E.
Date: 2017-10-28 17:46
Lang: en
Modified: 2018-07-03 03:17
Slug: apoyo-economico
Status: published
Title: Economic support

Since its birth, this website has used only free software and has been
free of ads. However, maintaining a website like this one requires both
time and money. [Donations]({filename}/pages/donaciones.en.md) for
encourage its development and improvement.

You can choose between different donation methods.

## Liberapay

<a href="https://liberapay.com/ConocimientosLibres/donate" role="button"><img alt="Donate" src="{static}/wp-content/uploads/pages/images/donate.svg"></a>
