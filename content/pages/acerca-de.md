Author: Jesús E.
Date: 2019-02-04 08:03
Lang: es
Modified: 2019-02-04 09:03
Slug: acerca-de
Status: hidden
Title: Acerca

## ¿Qué es LibreTube?

LibreTube es una plataforma de publicación de vídeos sin proceso de codificación
en el servidor. Es una web simple y sencilla para la presentación de vídeos.

## Colaboración

Puedes escribir un artículo de vídeo para este sitio web, hacer una
traducción o colaborar en el desarrollo. La información sobre cómo
hacerlo, se encuentra en el
[README del código fuente de la web][colaboracion].

[colaboracion]: https://libregit.org/heckyel/libretube/src/branch/master/README.md
[libertades]: https://www.gnu.org/philosophy/free-sw.html
