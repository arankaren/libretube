Author: Jesús E.
Date: 2018-07-03 02:06
Lang: eo
Slug: apoyo-economico
Status: published
Title: Ekonomika subteno

Ekde ĝia nasko, ĉi tia retpaĝaro uzis nur liberan programaron kaj
estis sen anoncoj. Tamen, bonteni retpaĝaron kiel ĉi tia bezonas kaj
tempon kaj monon. [Donacoj]({filename}/pages/donaciones.eo.md) por
instigi ĝian ellaboradon kaj plibonigon.

## Liberapay

<a href="https://liberapay.com/ConocimientosLibres/donate" role="button"><img alt="Donaci" src="{static}/wp-content/uploads/pages/images/donaci.svg"></a>
