## Descripción

Este repositorio contiene el sitio web LibreTube, ubicado en
<https://lablibre.tuxfamily.org/>.

## Generar LibreTube

### Instalación de dependencias

Puedes generar LibreTube en tu ordenador. Para ello necesitas tener
los siguientes programas instalados:
* [Pelican](https://github.com/getpelican/pelican). Es el generador de páginas
* [Python](http://python.org/). Es el lenguaje de programación en el que
  está escrito Pelican.
* [PHP](https://www.php.net/). Lenguaje de programación usado para el buscador
  y formulario de comentarios.
* [Markdown](https://pypi.python.org/pypi/Markdown/). Markdown
  es el lenguaje de marcado ligero en el que están escritos los
  artículos y páginas.
* [Jinja2](https://github.com/sobolevn/jinja2-git). Este complemento se
  utiliza para representar hash de confirmación en plantillas jinja2.
* [Babel](https://pypi.org/project/Babel/). Para la traducción del tema
  de la página.
* [BeautifulSoup4](https://pypi.python.org/pypi/beautifulsoup4/).
  Requerido por el complemento tipue-search.

Instalar `virtualenv` con:

Distros basadas en Debian:

`sudo apt install virtualenv`

Distros basadas en Arch:

`sudo pacman -S python-virtualenv`

No es realmente necesario, pero es muy recomendable instalar [GNU
Make](https://gnu.org/software/make). En la siguiente sección se asume
que tienes Make instalado.

+ En sistemas operativos basados en Arch se
  puede instalar con la instrucción:

        sudo pacman -S make

+ En sistemas operativos basados en Debian se
  puede instalar con la instrucción:

        sudo apt install make

### Generar el contenido

Tras instalar las dependencias, puedes generar el blog ejecutando las
siguientes órdenes:
1. `git clone https://notabug.org/heckyel/libretube`
2. `cd libretube`
3. `virtualenv -p python3 venv`
4. `source venv/bin/activate`
5. `pip install -U -r requirements.txt`
6. `(cd libretube-theme && make compile)`
7. `cp -v Makefile.example Makefile`
8. `make html`

Tras completar estos pasos, el blog estará disponible en la carpeta
`output`. Abre el archivo `output/index.html` con tu navegador favorito
para ver el blog.

## Colaboración

### Escribir un artículo

Si quieres publicar un artículo en LibreTube, puedes realizar un *pull
request* o [mandarme el artículo por correo
electrónico](mailto:heckyel@riseup.net). Si el artículo es
interesante, puede que lo acepte.

Si no sabes cómo funciona Pelican, puedes mándame el artículo por correo
sin preocuparte por el formato. Si quieres hacer un *pull request*,
debes utilizar los metadatos de Pelican y escribir el artículo en
[Markdown](https://es.wikipedia.org/wiki/Markdown). A continuación
se muestra un ejemplo.

```
Author: Nombre de la autora
Category: Python
Date: 2017-12-03 10:20
Slug: titulo-del-artículo
Tags: pelican, publishing
Title: Título del artículo

Este es el contenido del artículo. Puedo usar la sintaxis de
[Markdown](https://es.wikipedia.org/wiki/Markdown)
```

El contenido de LibreTube se encuentra bajo dominio público
([CC0 1.0 Universal](https://creativecommons.org/publicdomain/mark/1.0/)).
Si quieres publicar tu artículo usando otra licencia, indícalo.

Cuando escribas un artículo intenta utilizar
[etiquetas](https://lablibre.tuxfamily.org/tags/) y
[categorías](https://lablibre.tuxfamily.org/categories/) que ya existan, no
utilices el nombre de la categoría del artículo también como etiqueta.

Si necesitas alguna funcionalidad especial (como un vídeo, una imagen de
cabecera...) intenta utilizar complementos o funcionalidades que ya
estén presentes en el blog. Consulta ejemplos de artículos que hacen uso
de estos.

### Hacer una traducción

Copia el archivo que quieres traducir y añade el código del idioma (por
ejemplo `.de` para el alemán) antes de la extensión `.md`. Traduce el
contenido del artículo o la página. Cuando termines, cambia los
metadatos que hagan falta (título, autor, etc.) y añade el metadato
`Lang` con el valor del código del idioma de la traducción. A
continuación se muestra un ejemplo.

```
Author: nameauthor
Date: 2017-02-28 01:11
Lang: de
Slug: reflexión-sobre-los-medios-de-comunicación
Tags: Bücher, Fernsehen, freie Software, Internet, Kommentar, Medien
Title: Kommentar zu der Medien

Heutzutage gibt es viele Medien, die vorher nicht existierten. Das [...]
```

### Mejora de la página

También puedes mejorar la apariencia y funcionalidad de la página.
Simplemente crea un *pull request*. Antes de hacerlo, te recomendamos
conocer [cómo se organiza la estructura de
directorios](https://lablibre.tuxfamily.org/pages/estructura-de-directorios.html).
También es
recomendable comprobar si el código
<abbr title="HyperText Markup Language">HTML</abbr>
es válido ejecutando `make validate` en la carpeta del proyecto (debes
tener
<a href="https://www.gnu.org/software/make/"><abbr title="GNU's Not Unix">GNU</abbr> Make</a>
y [html5validator](https://pypi.python.org/pypi/html5validator)
instalados).
También es necesario tener instalado Java para que funcione `html5validator`:

    sudo pacman -S jdk8-openjdk jre8-openjdk-headless

Comprobar versión de Java:

```
$ java -version
openjdk version "1.8.0_121"
OpenJDK Runtime Environment (IcedTea 3.3.0) (Hyperbola GNU/Linux-libre build 8.u121_3.3.0-1.hyperbola4-x86_64)
OpenJDK 64-Bit Server VM (build 25.121-b13, mixed mode)
```

## Información de licencias

El contenido de este sitio web se encuentra bajo dominio público,
excepto donde se especifique lo contrario.

Todo el código es software libre; se encuentra bajo la licencia <a
href="https://www.gnu.org/licenses/agpl-3.0.html" rel="license"><abbr
title="Affero General Public License version 3">AGPLv3</abbr></a>, salvo
las siguientes excepciones:

- Licencias de JavaScript. La información sobre las licencias de
  JavaScript se encuentra en el archivo `content/pages/libreJS.md` en
  forma de [tabla preparada para ser leída por LibreJS](https://www.gnu.org/software/librejs/manual/librejs.html#JavaScript-Web-Labels).
- Todo lo que hay en el directorio `libretube-theme` se encuentra bajo la licencia <a href="https://www.gnu.org/licenses/gpl-3.0.html" rel="license"><abbr title="General Public License version 3">GPLv3</abbr></a>.
- Los siguientes complementos para Pelican (se encuentran en el
  directorio `plugins`):

    - **another_read_more_link**. Bajo la licencia [Apache License, Version 2.0](https://libregit.org/heckyel/libretube/raw/master/plugins/another_read_more_link/LICENSE).
    - **Neighbor Articles Plugin for Pelican**. Bajo la licencia <a href="https://www.gnu.org/licenses/gpl-3.0.html" rel="license"><abbr title="General Public License version 3">GPLv3</abbr></a>.
    - **pelican-css**. Bajo la licencia <a href="https://www.gnu.org/licenses/gpl-3.0.html" rel="license"><abbr title="General Public License version 3">GPLv3</abbr></a>.
    - **pelican-js**. Bajo la licencia <a href="https://www.gnu.org/licenses/gpl-3.0.html" rel="license"><abbr title="General Public License version 3">GPLv3</abbr></a>.

## Preguntas frecuentes

### ¿Dónde esta alojado el repositorio con el código fuente?

Está alojado en <https://libregit.org/heckyel/libretube>, y hay
copias en otros lugares:
- https://notabug.org/heckyel/libretube

### ¿Y el sistema de comentarios?

LibreTube utiliza el sistema de comentarios
[Hashover](https://github.com/jacobwb/hashover-next), el cual es
software libre:
- [Código fuente del lado del servidor](https://lablibre.tuxfamily.org/hashover-next/backend/source-viewer.php)
- [Código JavaScript](https://lablibre.tuxfamily.org/hashover-next/comments.php)
- [Diseño](https://libregit.org/heckyel/material-hashover)

### ¿Y los vídeos?

Los vídeos que contiene LibreTube no se encuentran
en este repositorio por varias razones:

- No es nada útil añadir los vídeos al repositorio, pues Git no está
  hecho para controlar los cambios en los vídeos. Además, normalmente no
  se suelen modificar los vídeos ya creados.
- Solo harían que este repositorio fuera más pesado aún.
- Algunos vídeos no se encuentran alojados en el servidor de LibreTube.

La siguiente tabla contiene algunos de los vídeos que se encuentran en LibreTube:

<table>
    <tr>
        <th>Título</th>
        <th>Fuente</th>
    </tr>
    <tr>
        <td>Citizenfour</td>
        <td><a href="https://archive.org/download/libreweb/citizenfour-spanish.webm">https://archive.org/download/libreweb/citizenfour-spanish.webm</a></td>
    </tr>
    <tr>
        <td>¿Cómo generar Freak Spot?</td>
        <td><a href="https://archive.org/download/libreweb/freakspot.webm">https://archive.org/download/libreweb/freakspot.webm</a></td>
    </tr>
    <tr>
        <td>Locutus de GNU</td>
        <td><a href="https://archive.org/download/libreweb/locutus.webm">https://archive.org/download/libreweb/locutus.webm</a></td>
    </tr>
    <tr>
        <td>Montar Android en Hyperbola</td>
        <td>
            <ul>
                <li><strong>1080p</strong>.<a href="https://archive.org/download/libreweb/android-uucp.webm">https://archive.org/download/libreweb/android-uucp.webm</a></li>
                <li><strong>480p</strong>.<a href="https://archive.org/download/libreweb/android-uucp-480.webm">https://archive.org/download/libreweb/android-uucp-480.webm</a></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>Prompt avanzado</td>
        <td><a href="https://archive.org/download/libreweb/0001-15599.webm">https://archive.org/download/libreweb/0001-15599.webm</a></td>
    </tr>
    <tr>
        <td>RMS en dominio digital</td>
        <td><a href="https://archive.org/download/libreweb/StallmanenDominioDigital.webm">https://archive.org/download/libreweb/StallmanenDominioDigital.webm</a></td>
    </tr>
    <tr>
        <td>Sin parar</td>
        <td><a href="https://archive.org/download/libreweb/sin_parar.webm">https://archive.org/download/libreweb/sin_parar.webm</a></td>
    </tr>
</table>
